from lhcb_ntuple_wizard.decaytreetuple_configurator import DecayTreeTupleConfigurator

branch_dict = {
    "Bplus": "[B+ -> (chi_c1(1P) -> (J/psi(1S) -> mu+ mu-) gamma) K+]CC",
    "chi_c": "[B+ -> ^(chi_c1(1P) -> (J/psi(1S) -> mu+ mu-) gamma) K+]CC",
    "Jpsi": "[B+ -> (chi_c1(1P) -> ^(J/psi(1S) -> mu+ mu-) gamma) K+]CC",
    "muplus": "[B+ -> (chi_c1(1P) -> (J/psi(1S) -> ^mu+ mu-) gamma) K+]CC",
    "muminus": "[B+ -> (chi_c1(1P) -> (J/psi(1S) -> mu+ ^mu-) gamma) K+]CC",
    "gamma": "[B+ -> (chi_c1(1P) -> (J/psi(1S) -> mu+ mu-) ^gamma) K+]CC",
    "kaon": "[B+ -> (chi_c1(1P) -> (J/psi(1S) -> mu+ mu-) gamma) ^K+]CC",
}
dtt = DecayTreeTupleConfigurator(name = "BuchicK", decay = "[B+ -> ^(chi_c1(1P) -> ^(J/psi(1S) -> ^mu+ ^mu-) ^gamma) ^K+]CC", branches = branch_dict, inputs = ["Phys/SelB2ChicKForPsiX0/Particles"])
for tool_class in [
    "TupleToolEventInfo",
    "TupleToolKinematic",
    "TupleToolPrimaries",
    "TupleToolPid",
    "TupleToolPropertime",
    "TupleToolRecoStats",
    "TupleToolTrackInfo",
    "TupleToolGeometry",
]:
    dtt.add_tool(tool_class)
dtt.remove_tool("TupleToolGeometry")
for particle in [key for key in branch_dict if key != "gamma"]:
    dtt.add_tool("TupleToolGeometry", branch = particle)
    if "u" in particle: # deliberate mistake, corrected later
        dtt.add_tool("LoKi::Hybrid::TupleTool", f"Nshared_{particle}", branch = particle, parameters = {"Variables": {"NSHAREDMU": "NSHAREDMU"}})
dtt.remove_tool("LoKi::Hybrid::TupleTool/Nshared_Bplus", branch = "Bplus")
for intermediate in ["chi_c", "Jpsi"]:
    dtt.remove_tool("TupleToolGeometry", branch = intermediate)
dtt.add_tool("LoKi::Hybrid::TupleTool", "AllExtraVars")
dtt.configure_tool("LoKi::Hybrid::TupleTool/AllExtraVars", parameters = {"Variables": {"ETA": "ETA", "PHI": "PHI"}})
dtt.add_tool("LoKi::Hybrid::TupleTool", "pi0veto", branch = "gamma", parameters = {"Variables": {"pi0veto": "PINFO(25030, -1)"}})
dtt.write_config("dtt_options.yml")
