from lhcb_ntuple_wizard import inputDicts
import yaml

inputDicts.writeDaVinciConfigInputYaml()
inputDicts.writeDttConfigInputYaml()

with open('DaVinciInput.yaml') as fp:
    DaVinciData = fp.read()
DaVinciData = yaml.safe_load(DaVinciData)
print(DaVinciData)

with open('dttConfiguratorInput.yaml') as fp:
    dttData = fp.read()
dttData = yaml.safe_load(dttData)
print(dttData)

dttDict = inputDicts.dttConfigInputDict()
DaVinciDict = inputDicts.DaVinciConfigInputDict()

print(dttDict)
print(DaVinciDict)

