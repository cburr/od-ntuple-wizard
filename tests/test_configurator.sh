here=$(dirname $(readlink -f $0))

AHREPO=/home/adam/work/utils/AnalysisHelpers

PYTHONPATH=$PYTHONPATH:$here/../src python3 $here/test_configurator.py

source /cvmfs/lhcb.cern.ch/lib/LbEnv

lb-run --container singularity DaVinci/v45r7 PYTHONPATH=\$PYTHONPATH:${AHREPO} \
	gaudirun.py \
		\$APPCONFIGOPTS/DaVinci/DataType-2018.py \
		${AHREPO}/examples/dtt_from_yaml.py

rm dtt_options.yml
