from AnalysisHelpers.decaytreetuple import configure_dtt
import yaml
config = yaml.safe_load(open("dtt_options.yml", "rb"))
dtt = configure_dtt(config, verbose = True)
DaVinci().UserAlgorithms += [dtt]
