import tkinter as tk
from tkinter import *
from itertools import chain
from tkinter.ttk import *
from tkinter.messagebox import showinfo
from decaylanguage import *
from PIL import ImageTk,Image  
from graphviz import Digraph
from tkinter import simpledialog
from lhcb_ntuple_wizard import stripping21
import yaml


app_window_x = 1000
app_window_y = 1000
lines = stripping21.lines
def extract_all(data, key):
	return set(chain(*(d[key] for d in data.values())))

def extract_keys(data):
	return set(chain(d for d in data.keys()))

def stream_select(event):
	# This works for now, but is not robust for having more than 2 widgets in this frame...
	#for i in range(0,len(frame.winfo_children())):
	#    if (i>0 and i<=len(frame.winfo_children())):
	#        frame.winfo_children()[i].destroy()
	for x in frame2.winfo_children():
		x.destroy()
	for x in frame3.winfo_children():
		x.destroy()
	for x in frame4.winfo_children():
		x.destroy()
	for x in frame5.winfo_children():
		x.destroy()
	stream_lines = []
	for key,value in lines.items():
		if value['streams'] == [stream_menu.get()]:
			stream_lines.append(key)
	line_label = Label(frame2,text = "Choose a stripping line")  
	line_label.pack(pady=5)
	line_variable = tk.StringVar(app)
	line_menu = Combobox(frame2, textvariable=line_variable,values = stream_lines,width=app_window_x )
	line_menu.pack(pady=10)
	line_menu.bind('<<ComboboxSelected>>', lambda event:line_select(event,line_menu))

def line_select(event,line_menu):
	for x in frame3.winfo_children():
		x.destroy()
	for x in frame4.winfo_children():
		x.destroy()
	for x in frame5.winfo_children():
		x.destroy()
	for x in frame6.winfo_children():
		x.destroy()
	decay_descriptors = []
	for key,value in lines.items():
		if (key == line_menu.get()):
			#print(value['decaydescriptors'])
			decay_descriptors = value['decaydescriptors']
	decay_label = Label(frame3,text = "Choose a decay descriptor")  
	decay_label.pack(pady=5)
	decay_variable = tk.StringVar(app)
	decay_menu = Combobox(frame3, textvariable=decay_variable,values = decay_descriptors, width=app_window_x )
	decay_menu.pack(pady=10)
	decay_menu.bind('<<ComboboxSelected>>', lambda event: decay_select(event,decay_menu, line_menu))

def decay_select(event, decay_menu, line_menu):
	for x in frame4.winfo_children():
		x.destroy()
	for x in frame5.winfo_children():
		x.destroy()
	for x in frame6.winfo_children():
		x.destroy()
	decay_descriptor = decay_menu.get()
	decay_descriptor = decay_descriptor[2:len(decay_descriptor)-2]
	decay_descriptor = decay_descriptor.replace('  ', ' ')
	# Grab inputs to set up the decay tree tuple branch dictionary and decay descriptor
	branch_names = decay_descriptor.split()
	print(decay_descriptor)
	print(branch_names)
	decay_modes = []
	decay_parts = []
	mothers = []
	mother = ''
	for i in range(0,len(branch_names)):
		# fails sometimes.. need to do better, will ultimately need to count the number of open and closed parenthases that have already occured up to a given element -- need to fix the issue of getting wrong parent during nested decays
		if branch_names[i] == "->":
			mother = branch_names[i-1]
		if branch_names[i] == "(" and branch_names[0:i].count("(") == branch_names[0:i].count(")")+1:
			mother = branch_names[0]

		#if branch_names[0:i].count("(") == branch_names[0:i].count(")") or ( branch_names[0:i].count("(") == branch_names[0:i].count(")")+1 and branch_names[0:i].count("->") != branch_names[0:i].count("(")):
		if branch_names[0:i].count("(") == branch_names[0:i].count(")"):
			mother = branch_names[0]

		if branch_names[i] != '->' and branch_names[i] != '(' and branch_names[i] != ')':
			if i==0:
				continue
			decay_modes.append(DecayMode('',branch_names[i]))  
			mothers.append(mother) 
			decay_parts.append(branch_names[i])

	#decay_edges = [(x,y) for (x,y) in zip(mothers,decay_parts)]
	#decay_chain_dict = dict(zip(mothers,decay_modes))	
	print(mothers)
	print(decay_parts)

	dups = []
	counters = []
	[dups.append(x) for x in decay_parts if x not in dups and decay_parts.count(x) > 1]

	labels = []
	for i in range(len(dups)):
		counters.append(1)
	for b in decay_parts:
		q=b
		if dups.count(b) >= 1:            # This will only work when there are duplicates of one species
			dupIndex = dups.index(b)
			q = q + str(counters[dupIndex])  
			counters[dupIndex] += 1
		labels.append(q)

	decay_edges = [(x,y,z) for (x,y,z) in zip(mothers,decay_parts,labels)]	
	print(decay_edges)
        
	dot = Digraph(comment=f'{decay_descriptor} Decay Tree')
	count = 0
	for (x,y,z) in decay_edges:
		count +=1
		dot.node(z,y)
		dot.edge(x,z)

	dot.render(filename="decay_tree_graph", format="png", view=False, cleanup=True)
		
	img = ImageTk.PhotoImage(Image.open("decay_tree_graph.png"))  
	label1 = tk.Label(image=img)
	label1.image = img
	#Label(app, text= 'test').pack(side = TOP)
	display_tools = Button(frame4, image = img, command= lambda: DisplayTools(decay_menu, line_menu))
	display_tools.pack( pady = 40 )

def DisplayTools(decay_menu, line_menu):
	for x in frame5.winfo_children():
		x.destroy()
	for x in frame6.winfo_children():
		x.destroy()
	tuple_tools = [
                    # default tool list in DaVinci v45r7
                    # TODO: load this from a file generated on deployment
                    "TupleToolKinematic",
                    "TupleToolPid",
                    "TupleToolANNPID",
                    "TupleToolGeometry",
                    "TupleToolEventInfo"
                ]
	label = Label(frame5,text = "Global tuple tools")  
	listbox = Listbox(frame5)
	for i in range(0,len(tuple_tools)):
		listbox.insert(i+1, tuple_tools[i])

	add_tool = Button(frame5, text ="Add Tool", command = lambda: AddTool(listbox))
	add_tool.pack(side = BOTTOM, pady=20)
	configure_tool = Button(frame5, text ="Conrigure Tool", command = lambda: ConfigureTool(listbox))
	configure_tool.pack(side = BOTTOM, pady=20 )
	remove_tool = Button(frame5, text ="Remove Tool", command = lambda: RemoveTool(listbox))
	remove_tool.pack(side = BOTTOM, pady=20)

	submit = Button(frame6, text ="Generate Options Files", command = lambda: GenerateOptions(listbox, decay_menu, line_menu))
	submit.pack(side = BOTTOM, pady=80 )
	listbox.pack(side=BOTTOM)
	label.pack(side=BOTTOM)  
	
def AddTool(listbox):
	#listbox.insert(listbox.size()+1,"temp");
	tool_textbox = Text(frame5)
	added_tool = simpledialog.askstring(title="Add Tuple Tool",prompt="What tool would you like to add?:")
	listbox.insert(listbox.size()+1,added_tool);

def ConfigureTool(listbox):
	print(listbox.get(listbox.curselection()))

def RemoveTool(listbox):
	listbox.delete(listbox.curselection())

def GenerateOptions(listbox, decay_menu, line_menu):
	global_tuple_tools = []
	for x in listbox.get(0,listbox.size()):
		global_tuple_tools.append(x)
			
	userInputDict = {'dataset' : 'stripping21', 'line_name' : line_menu.get(), 'decay_descriptor' :decay_menu.get(), 'global_tuple_tools' : global_tuple_tools, 'simulation' : False, 'jet_reco' : False, 'stream' : stream_menu.get()}
	with open("userInput.yaml", 'w') as userInputYaml:
		yaml.dump(userInputDict, userInputYaml, default_flow_style = False)
	import analysisOptionsGen
	exec(open('analysisOptionsGen.py').read())
	app.destroy()

	
#stripping_lines = extract_keys(lines)
print(f"Found {len(stripping21.lines)} for Stripping 21 ({stripping21.description})")
stream_dict = extract_all(stripping21.lines, "streams")
print(f"Known streams are: {stream_dict}")
decaydescriptors = extract_all(stripping21.lines, "decaydescriptors")
print(f"{len(decaydescriptors)} possible decays")

streams = []
for key in stream_dict:
	streams.append(key)

app = tk.Tk()
app.geometry(f'{app_window_x}x{app_window_y}')

frame = Frame(app)
frame2 = Frame(app)
frame3 = Frame(app)
frame4 = Frame(app)
frame5 = Frame(app)
frame6 = Frame(app)
frame.pack(side="top", fill="both")
frame2.pack(side="top", fill="both")
frame3.pack(side="top",  fill="both")
frame4.pack(side="top", expand=True, fill="both")
frame5.pack(side="left", expand=True, fill="both")
frame6.pack(side="right", expand=True, fill="both")

stream_label = Label(frame,text = "Choose a data stream")  
stream_label.pack(pady=5)
stream_variable = tk.StringVar(app)
stream_menu = Combobox(frame, textvariable=stream_variable,values = sorted(streams), width=app_window_x )
stream_menu.pack(pady=10)

stream_menu.bind('<<ComboboxSelected>>', stream_select)


app.mainloop()

