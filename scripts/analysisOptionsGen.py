from lhcb_ntuple_wizard import RootinTESGen, EventPreFilterGen, jetRecoGen, DataTypeGen, inputDicts
from lhcb_ntuple_wizard import decaytreetuple_configurator 
from yaml import safe_load

with open("userInput.yaml") as fp:
    data = fp.read()
data = safe_load(data)
jetReco = data['jet_reco']
tupleName = data['line_name']
tupleName = tupleName.replace('Stripping','')
tupleName = tupleName.replace('Line','')
tupleName += 'Tuple'

# Output in temp directory -- intermediate step, so user should not touch the output #
inputDicts.writeDttConfigInputYaml()
inputDicts.writeDaVinciConfigInputYaml()

RootinTESGen.writeOptionsFile(outfilename = 'options/RootinTES.py')
EventPreFilterGen.writeOptionsFile(outfilename = 'options/EventPreFilter.py')
DataTypeGen.writeOptionsFile(outfilename = 'options/DataType.py')
if(jetReco):
    jetRecoGen.writeOptionsFile(outfilename = 'options/jetReco.py')

with open("temp/dttConfiguratorInput.yaml") as fp:
    dttData = fp.read()
dttData = safe_load(dttData)

dttConfig = decaytreetuple_configurator.DecayTreeTupleConfigurator( name = tupleName, decay = dttData['decay'], branches = dttData['branches'], inputs = [dttData['TES']])
dttConfig.write_config('options/dtt_options.yml')








