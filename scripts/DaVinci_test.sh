here=$(dirname $(readlink -f $0))

AHREPO=../../../analysishelpers

PYTHONPATH=$PYTHONPATH:$here/../src python3 $here/analysisOptionsGen.py

source /cvmfs/lhcb.cern.ch/lib/LbEnv
cd options

#test to see if this is the correct order -- need to know for options file list builder
lb-run --allow-containers DaVinci/v45r7 PYTHONPATH=\$PYTHONPATH:${AHREPO} \
  gaudirun.py \
  jetReco.py \
  ${AHREPO}/examples/dtt_from_yaml.py \
  RootinTES.py \
  DataType.py \
  EventPreFilter.py 

cd ../ 
rm options/*
rm temp/*
