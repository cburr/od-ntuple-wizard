#!/bin/bash
if [ ! $# -eq 2 ]
then
	echo "Usage: $(basename $0) <DaVinci version> <class name>"
	exit 1
fi

export VERSION=$1
CLASS=$2
DOXDIR=$(jq -r ".\"davinci/${VERSION}\"" /eos/project/l/lhcbwebsites/www/doxygen/docs/db.json)

PAGE=$(strings /eos/project/l/lhcbwebsites/www/doxygen/${DOXDIR}/search/search.idx | grep -A 1 "^class ${CLASS}$" | tail -1)

echo "https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/${VERSION}/${PAGE}"
