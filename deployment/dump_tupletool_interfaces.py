#!/usr/bin/env python

import Configurables
from Configurables import __all__ as all_configurables
all_tupletools = filter(lambda t: "TupleTool" in t, all_configurables)

interfaces = {t: {} for t in all_tupletools}

import pydoc
for tool_name in all_tupletools:
	tool = getattr(Configurables, tool_name)()
	interfaces[tool_name] = tool.getPropertiesWithDescription()

import json
for tool_name, interface in interfaces.iteritems():
	tool = getattr(Configurables, tool_name)()
	with open("tupletool_interfaces/"+tool_name+".json", "w") as f:
		keys_to_delete = []
		for key in interface:
			try:
				json.dumps(interface[key])
			except TypeError:
				print("Cannot serialise "+tool_name+"."+key)
				interface[key] = ("<unconfigurable>", interface[key].__repr__())
			interface[key] = (tool.getProp(key), interface[key][1])
			if interface[key][1] is not None:
				if "unknown owner type" not in interface[key][1]:
					# My intention here is to remove all the properties that inherit
					# from parent classes like AlgTool/GaudiTool/GaudiHistoTool
					keys_to_delete += [key]
				else:
					interface[key] = (interface[key][0], interface[key][1].replace("[unknown owner type]", "").strip())
			else:
				interface[key] = (interface[key][0], "No documentation")
		for key in keys_to_delete:
			del interface[key]
		json.dump(interface, f, indent=4)
