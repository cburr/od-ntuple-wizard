#!/usr/bin/python3

import sys, json, tabulate

with open(sys.argv[1], "r") as f:
	interface = json.load(f)

table = []

for key,(val, doc) in interface.items():
	typ = type(val).__name__
	if typ == "str" and len(val) == 0:
		val = '""'
	table += [[f"`{key}`", f"`{typ}`", f"`{val}`", doc]]

table_string = tabulate.tabulate(table, headers=["Name", "Type", "Default value", "Description"], tablefmt="github")

tool_name = sys.argv[1].split("/")[-1].replace(".json", "")
with open(sys.argv[2], "r") as f:
	doxygen_link = f.read().strip()

print(f"""---
layout: page
title: {tool_name}
categories: hidden
---
[*back to list of TupleTools*](/tupletools)

See the [Doxygen page]({doxygen_link}#details) for more information.

{table_string}
""")
