# Deployment scripts

This directory contains the scripts that download the necessary metadata used by the Wizard.
The Makefile should assist in running them, assuming you have /eos and /cvmfs mounted.

```
./dump_tupletool_interfaces.sh
make -j
```

To avoid all developers having to run this themselves, the output can be copied from this location:
```
/eos/lhcb/user/a/admorris/wizard/metadata/DaVinci_v45r7/
```
