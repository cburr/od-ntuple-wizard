The lhcb_ntuple_wizard package will allow users to generate ntuples without knowledge of the LHCb software stack. It will read in intuitive user inputs and produce necessary options and configuration for launching analysis productions, returning ntuples to the user. This will be available to external users as a part of LHCb's open data efforts as well as members of the LHCb collaboration. More information can be found [here](https://lhcb-dpa.web.cern.ch/lhcb-dpa/wp6/ntupling-wizard.html). 

A flow chart of the lhcb_ntuple_wizard is shown below

![](docs/docs_wizard.png)

This is related to the src/lhcb_ntuple_wizard code as follows:

- [decaytreetuple_configurator.py](https://gitlab.cern.ch/dfitzger/od-ntuple-wizard/-/blob/master/src/lhcb_ntuple_wizard/decaytreetuple_configurator.py) --> generate DTTOptions.yml (read in by analysis productions to generate options file for decay tree tuple with necessary configuration) 
- [ROOTinTESGen.py](https://gitlab.cern.ch/dfitzger/od-ntuple-wizard/-/blob/master/src/lhcb_ntuple_wizard/RootinTESGen.py) --> generate RootinTES.py options file (only for mdst streams)
- [EventPreFilterGen.py](https://gitlab.cern.ch/dfitzger/od-ntuple-wizard/-/blob/master/src/lhcb_ntuple_wizard/EventPreFilterGen.py) --> generate EventPreFilter.py options file 
- [jetRecoGen.py](https://gitlab.cern.ch/dfitzger/od-ntuple-wizard/-/blob/master/src/lhcb_ntuple_wizard/jetRecoGen.py) --> generate options file for custom jet reconstruction 
- [inputDicts.py](https://gitlab.cern.ch/dfitzger/od-ntuple-wizard/-/blob/master/src/lhcb_ntuple_wizard/inputDicts.py) --> generate input dictionaries/yaml files necessary to be input to...
    - decay tree tuple configuration
        - decaytreetuple_configurator.py 
    - DaVinci configuration
        - function to generate ROOTinTES.py
        - function to generate EventPreFilter.py
- [extractDttInputs.py](https://gitlab.cern.ch/dfitzger/od-ntuple-wizard/-/blob/master/src/lhcb_ntuple_wizard/extractDttInputs.py) --> contains functions to grab necessary information for inputDicts.py -- current implementation requires only a stripping line name (information extracted from [stripping21.py](https://gitlab.cern.ch/dfitzger/od-ntuple-wizard/-/blob/master/src/lhcb_ntuple_wizard/stripping21.py))
