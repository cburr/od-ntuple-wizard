import yaml
from lhcb_ntuple_wizard import extractDttInputs
	
def dttConfigInputDict(filename = "userInput.yaml"):
    with open(filename) as fp:
        data = fp.read()
    data = yaml.safe_load(data)
    keys = list(data.keys())
    if (keys.count('decay_descriptor') != 0):
        decay_descriptor = data['decay_descriptor']
    else:
        decay_descriptor = extractDttInputs.extractDecay(lineName)
    mc = data['simulation']
    lineName = data['line_name']
    branches = extractDttInputs.extractBranches(lineName,decay_descriptor)
    TESlocation = extractDttInputs.extractTESLocation(lineName,mc)
    dttConfiguratorInputDict = { 'decay' : decay_descriptor, 'branches' : branches, 'TES' : TESlocation }
    return dttConfiguratorInputDict

def writeDttConfigInputYaml(filename = "userInput.yaml"):
    dttConfiguratorInputDict = dttConfigInputDict(filename)
    with open("temp/dttConfiguratorInput.yaml", 'w') as dttConfiguratorInputYaml:
        yaml.dump(dttConfiguratorInputDict, dttConfiguratorInputYaml, default_flow_style = False)

def DaVinciConfigInputDict(filename = "userInput.yaml"):
    with open(filename) as fp:
        data = fp.read()
    data = yaml.safe_load(data)
    mc = data['simulation']
    lineName = data['line_name']
    decision = lineName + 'Decision'
    stream = extractDttInputs.extractStream(lineName,mc) 
    dataType = extractDttInputs.extractDataType()
    DaVinciInputDict = { 'stream' : stream, 'decision' : decision, 'dataType' : dataType, 'simulation' : mc }
    return DaVinciInputDict

def writeDaVinciConfigInputYaml(filename = "userInput.yaml"):
    DaVinciInputDict = DaVinciConfigInputDict(filename)
    with open("temp/DaVinciInput.yaml", 'w') as DaVinciInputYaml:
        yaml.dump(DaVinciInputDict, DaVinciInputYaml, default_flow_style = False)


