from yaml import safe_load

def writeOptionsFile(infilename = 'temp/DaVinciInput.yaml', outfilename = 'options/EVentPreFilter.py'):
    with open(infilename) as fp:
        data = fp.read()
    data = safe_load(data)	
    decision = data['decision']

    EventPreFilterContent = f'''from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (STRIP_Code = "HLT_PASS_RE('{decision}')")
from Configurables import DaVinci
DaVinci().EventPreFilters = fltrs.filters('Filters')
'''
    f = open(outfilename,'w')
    f.write(EventPreFilterContent)
    f.close()

