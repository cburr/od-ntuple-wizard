import yaml, sys
from copy import deepcopy

class DecayTreeTupleConfigurator:
    def __init__(self, decay, branches, inputs = [], name = "DecayTreeTuple"):
        self.name = f"DecayTreeTuple/{name}"
        self.config = {
            self.name: {
                "inputs": inputs,
                "decay": decay,
                "branches": {branch: {"decay": branches[branch]} for branch in branches},
                "tuple_tools": [
                    # default tool list in DaVinci v45r7
                    # TODO: load this from a file generated on deployment
                    "TupleToolKinematic",
                    "TupleToolPid",
                    "TupleToolANNPID",
                    "TupleToolGeometry",
                    "TupleToolEventInfo"
                ]
            }
        }
    def get_target(self, branch = None):
        if not branch:
            return self.config[self.name]
        else:
            return self.config[self.name]["branches"][branch]

    def add_tool(self, tool_class, tool_name = "", branch = None, parameters = {}):
        target = self.get_target(branch)
        if "tuple_tools" not in target:
            target["tuple_tools"] = []
        if len(tool_name) == 0:
            tool_string = tool_class
        else:
            tool_string = "/".join([tool_class, tool_name])
        try:
            self.find_tool(tool_string, branch)
            print(f"tuple_tools already contains {tool_string}", file = sys.stderr)
        except KeyError:
            target["tuple_tools"] += [{tool_string: parameters}]

    def find_tool(self, tool_string, branch = None):
        target = self.get_target(branch)
        for i,tool in enumerate(target["tuple_tools"]):
            if tool_string in tool:
                return i
        raise KeyError(f"{tool_string} not found")

    def configure_tool(self, tool_string, branch = None, parameters = {}):
        target = self.get_target(branch)
        index = self.find_tool(tool_string, branch)
        target["tuple_tools"][index] = {tool_string: parameters}

    def remove_tool(self, tool_string, branch = None):
        target = self.get_target(branch)
        index = self.find_tool(tool_string, branch)
        target["tuple_tools"].pop(index)

    def build_config(self):
        output_config = deepcopy(self.config)
        configurables = [output_config[self.name]] + [output_config[self.name]["branches"][key] for key in output_config[self.name]["branches"]]
        for j, target in enumerate(configurables):
            if "tuple_tools" in target:
                if len(target["tuple_tools"]) == 0:
                    configurables[j].pop("tuple_tools")
                    continue
                print(f"Squelching empty tools in {target['decay']}", file = sys.stderr)
                for i,tool in enumerate(target["tuple_tools"]):
                    if isinstance(tool, dict):
                        assert(len(tool) == 1)
                        tool_name = [key for key in tool][0]
                        if len(tool[tool_name]) == 0:
                            print(f"    {tool_name} -> str", file = sys.stderr)
                            configurables[j]["tuple_tools"][i] = tool_name
        return output_config

    def write_config(self, filename = None):
        if filename is None:
            filename = f"{self.name}.yaml"
        with open(filename, "w") as f:
            yaml.safe_dump(self.build_config(), f)

    def print_config(self):
        yaml.safe_dump(self.build_config(), sys.stdout)
