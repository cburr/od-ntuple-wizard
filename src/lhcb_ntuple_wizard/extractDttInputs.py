from lhcb_ntuple_wizard import stripping21

def extractDataType():
	return stripping21.datatype

def extractStream(lineName,mc):
    line = stripping21.lines[lineName]
    streams = line['streams']
    if mc:
        stream = 'AllStreams'
    else:
        stream = streams[0]
    return stream

def extractTESLocation(lineName,mc):
    mdst = 0
    line = stripping21.lines[lineName]
    lineTESName = lineName.replace('Stripping','')
    streams = line['streams']
    if mc:
        stream = 'AllStreams'
    else:
        stream = streams[0]
        if stream.endswith('.dst'):
            stream = stream.replace('.dst','')
        elif stream.endswith('.mdst'):
            mdst = 1
            stream = stream.replace('.mdst','')
    if mdst:
        TESlocation = '/Phys/{0}/Particles'.format(lineTESName)
    else:
        TESlocation = '/Event/{0}/Phys/{1}/Particles'.format(stream,lineTESName)

    return TESlocation

def extractDecay(lineName):
    line = stripping21.lines[lineName]
    decay_descriptors = line['decaydescriptors'] 
    decay_descriptor = decay_descriptors[0]
    decay_descriptor = decay_descriptor[2:len(decay_descriptor)-2]
    decay_descriptor = decay_descriptor.replace('  ', ' ')
    # Grab inputs to set up the decay tree tuple branch dictionary and decay descriptor
    branch_names = decay_descriptor.split()
    for i in range(0,branch_names.count('(')):
        branch_names.remove('(')
    for i in range(0,branch_names.count(')')):
        branch_names.remove(')')
    for i in range(0,branch_names.count('->')):
        branch_names.remove('->')
    #determine if the decay has a charge conjugate (may need revision)
    cc = 0
    parts = ['pi','K','p','e','mu','tau']
    for p in parts:
        pp = p + '+'
        if p == 'p':
            pm = p + '~-'
        else:
            pm = p + '-'
        if branch_names.count(pp) != branch_names.count(pm):
            cc = 1
    if cc:
        decay_descriptor = '[' + decay_descriptor + ']CC'
    else:
        decay_descriptor = '(' + decay_descriptor + ')'

    temp_string = ''
    for i in decay_descriptor:
        if i==' ':
            i+='^'
        temp_string += i
    decay_descriptor = temp_string.replace("( ^","( ")
    decay_descriptor = decay_descriptor.replace("^->","->")
    decay_descriptor = decay_descriptor.replace("^)",")")
    return decay_descriptor

def extractBranches(lineName, decay_descriptor):
    line = stripping21.lines[lineName]
    #decay_descriptors = line['decaydescriptors'] 
    #decay_descriptor = decay_descriptors[0]
    decay_descriptor = decay_descriptor[2:len(decay_descriptor)-2]
    decay_descriptor = decay_descriptor.replace('  ', ' ')

    # Grab inputs to set up the decay tree tuple branch dictionary and decay descriptor
    branch_names = decay_descriptor.split()
    for i in range(0,branch_names.count('(')):
        branch_names.remove('(')
    for i in range(0,branch_names.count(')')):
        branch_names.remove(')')
    for i in range(0,branch_names.count('->')):
        branch_names.remove('->')

    #determine if the decay has a charge conjugate (may need revision)
    cc = 0
    parts = ['pi','K','p','e','mu','tau']
    for p in parts:
        pp = p + '+'
        if p == 'p':
            pm = p + '~-'
        else:
            pm = p + '-'
        if branch_names.count(pp) != branch_names.count(pm):
            cc = 1

    if cc:
        decay_descriptor = '[' + decay_descriptor + ']CC'
    else:
        decay_descriptor = '(' + decay_descriptor + ')'

    temp_string = ''
    for i in decay_descriptor:
        if i==' ':
            i+='^'
        temp_string += i

    decay_descriptor = temp_string.replace("( ^","( ")
    decay_descriptor = decay_descriptor.replace("^->","->")
    decay_descriptor = decay_descriptor.replace("^)",")")

    carrots = [ i for i in range(len(decay_descriptor)) if decay_descriptor.startswith('^',i)]
    carrots.insert(0,0)
    
    dups = []
    counters = []
    branchKey= []
    branchVal = []

    [dups.append(x) for x in branch_names if x not in dups and branch_names.count(x) > 1]

    for i in range(len(dups)):
        counters.append(1)
    for b in branch_names:
        q=b
        if dups.count(b) >= 1:            # This will only work when there are duplicates of one species
            dupIndex = dups.index(b)
            q = q + str(counters[dupIndex])  
            counters[dupIndex] += 1

        q = q.replace("(","_")
        q = q.replace(")","_")
        q = q.replace('*','star')
        q = q.replace('~','bar')
        q = q.replace('+','plus')
        q = q.replace('-','minus')
        branchKey.append(q)

    for i in range(len(branch_names)):
        if i==0:
            decayPart = decay_descriptor.replace("^","")
        else:
            decayPart = ''.join([decay_descriptor[j] for j in range(len(decay_descriptor)) if carrots.count(j)!=1 or carrots[i]==j or j==0]) 
        branchVal.append(decayPart)

    branches = dict(zip(branchKey,branchVal))
    return branches

