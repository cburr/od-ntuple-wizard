from yaml import safe_load

def writeOptionsFile(infilename = 'temp/DaVinciInput.yaml', outfilename = 'options/DataType.py'):
    with open(infilename) as fp:
        data = fp.read()
    data = safe_load(data)	
    dataType = data['dataType']

    DataTypeContent = f'''from Configurables import DaVinci
DaVinci().DataType = '{dataType}'
''' 
    f = open(outfilename,'w')
    f.write(DataTypeContent)
    f.close()

    

    
