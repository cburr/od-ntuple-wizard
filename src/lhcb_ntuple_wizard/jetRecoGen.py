from yaml import safe_load

def writeOptionsFile(infilename = 'userJetInput.yaml', outfilename = 'options/jetReco.py'):	
    with open(infilename) as fp:
        data = fp.read()
    data = safe_load(data)
    jetR = data['jetR']
    JEC = data['jetEC']
    jetID = data['jetID']
    jetPtMin = data['jetPtMin']
    jetRecoAlg = data['jetRecoAlg']

    jetRecoOptions = f'''from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from Configurables import DaVinci
from PhysSelPython.Wrappers import DataOnDemand

pflow    = ParticleFlowConf("PF")
jetmaker = JetMakerConf("PFJets", R = {jetR}, JetEnergyCorrection = {JEC}, JetIDCut = {jetID}, PtMin = {jetPtMin}, algtype = '{jetRecoAlg}')
DaVinci().UserAlgorithms += pflow.algorithms
DaVinci().UserAlgorithms += jetmaker.algorithms
'''
    f = open(outfilename,'w')
    f.write(jetRecoOptions)
    f.close()
