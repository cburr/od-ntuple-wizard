from yaml import safe_load

def writeOptionsFile(infilename = 'temp/DaVinciInput.yaml', outfilename = 'options/RootinTES.py'):
    with open(infilename) as fp:
        data = fp.read()
    data = safe_load(data)	
    stream = data['stream']

    mdst = 0
    if stream.endswith('.dst'):
        stream = stream.replace('.dst','')
    elif stream.endswith('.mdst'):
        mdst = 1
        stream = stream.replace('.mdst','')	

    if mdst:
        RootinTESContent = f'''from Configurables import DaVinci
DaVinci().RootInTES = '/Event/{stream}'
''' 
        f = open(outfilename,'w')
        f.write(RootinTESContent)
        f.close()

    

    
